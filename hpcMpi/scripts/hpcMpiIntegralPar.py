#!/usr/bin/env python3

import hpcMpi
import sys
import time as t
import numpy
from mpi4py import MPI

if __name__ == '__main__':
    comm = MPI.COMM_WORLD
    worldRank = comm.Get_rank()
    worldSize = comm.Get_size()
    step = 1e-3
    par = 0.25
    if len(sys.argv) == 2:
        step = float(sys.argv[1])
        par = 1.0 / worldSize

    node_result = numpy.empty(1,'d')
    all_results = numpy.empty(1,'d')
    t0 = t.time()	
    
    # compute
    if worldRank == 0:
        node_result[0] = hpcMpi.compute(hpcMpi.fPi, 0, par, step)
    else:
        node_result[0] = hpcMpi.compute(hpcMpi.fPi,worldRank*par,(worldRank+1)*par,step)

    comm.Reduce(node_result,all_results,op=MPI.SUM)
    t1 = t.time()
	
	
    # output result
    time = t1 - t0
    if worldRank == 0:
        print(step,worldSize, all_results, time)
