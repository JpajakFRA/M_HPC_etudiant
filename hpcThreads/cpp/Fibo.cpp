#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

namespace Fibo {

  // calcule le Nieme terme de la suite de "Fibonacci modulo 42"
  // precondition : N >= 0
  int FibonacciMod42(int N) {
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++) {
      int tmp = f_curr;
      f_curr = (f_curr + f_prec) % 42;
      f_prec = tmp;
    }
    return f_curr;
  }

  //////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
  void calculerTout(std::vector<int> &data) {
    // effectue tous les calculs
    for (unsigned i=0; i<data.size(); i++) {
      data[i] = FibonacciMod42(i);
    }
  };

  std::vector<int> fiboSequentiel(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calcule les donnees sequentiellement
    calculerTout(data);
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void calculerPartiellement(std::vector<int> &data,int indice_depart,int indice_stop){
	for(int i = indice_depart;i < indice_stop;i++){
		data[i] = FibonacciMod42(i);
	}
}

  std::vector<int> fiboBlocs(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, par bloc
    std::thread t1 (calculerPartiellement,std::ref(data),0,nbData/2);
    std::thread t2 (calculerPartiellement,std::ref(data),nbData/2,nbData);
    t1.join();
    t2.join();
    
    return data;
  }

  //////////////////////////////////////////////////////////////////////

void calculerPartiellement2(std::vector<int> &data,int indice_depart,int indice_stop){
	for(int i = indice_depart;i < indice_stop;i+=2){
		data[i] = FibonacciMod42(i);
	}
}

  std::vector<int> fiboCyclique2(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, cycliquement
    std::thread t1 (calculerPartiellement2,std::ref(data),0,nbData);
    std::thread t2 (calculerPartiellement2,std::ref(data),1,nbData);
    t1.join();
    t2.join();
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void calculerPartiellementN(std::vector<int> &data,int indice_depart,int indice_stop,int pas){
	for(int i = indice_depart;i < indice_stop;i+=pas){
		data[i] = FibonacciMod42(i);
	}
  }

  std::vector<int> fiboCycliqueN(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    std::vector<std::thread> thr;
    // calculer sur N threads, cycliquement
    for(int i = 0;i<nbProc;i++){
		thr.push_back(std::thread (calculerPartiellementN,std::ref(data),i,nbData,nbProc));
	}
	for(int i = 0;i<nbProc;i++){
		thr[i].join();
	} 
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void fiboCycliqueNFake(int nbData, int nbProc) {
    // calculer sur N threads, cycliquement, en ignorant le résultat
    std::vector<int> data(nbData); 
    std::vector<std::thread> thr;
    
    for(int i = 0;i<nbProc;i++){
		thr.push_back(std::thread (calculerPartiellementN,std::ref(data),i,nbData,nbProc));
	}
	for(int i = 0;i<nbProc;i++){
		thr[i].join();
	} 
  }

}  // namespace Fibo

