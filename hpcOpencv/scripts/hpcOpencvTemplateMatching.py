#!/usr/bin/env python3


import time
import sys
import matplotlib
from matplotlib import pyplot
import cv2 as cv
import numpy

if __name__ == '__main__':

    # arguments
    if len(sys.argv) != 3:
        print("usage:", sys.argv[0], "<filename> <filename>")
        sys.exit(-1)
    VIDEOSOURCE = sys.argv[1]
    TEMPLATE = sys.argv[2]
  
    # load input image
    cap = cv.VideoCapture(VIDEOSOURCE)
    template = cv.imread(TEMPLATE)
    w, h, c = template.shape
      
    while(cap.isOpened()):
        ret, frame = cap.read()
        if not ret:
            break
        res = cv.matchTemplate(frame,template,cv.TM_CCOEFF_NORMED)
        min_val,max_val,min_loc,max_loc=cv.minMaxLoc(res)

        position = numpy.where(res >= 0.7)
        for point in zip(*position[::-1]):
            cv.rectangle(frame,point,(point[0] + h,point[1] + w),(0,0,255,255),1)
        
        cv.imshow('tracking',frame)
        print("frame = ", cv.CAP_PROP_FRAME_COUNT,",maxVal = ", max_val, ", maxLoc = ",max_loc)     
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
