#!/usr/bin/env python3

import cv2 as cv
import time
import sys

if __name__ == '__main__':

    # arguments
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<filename>")
        sys.exit(-1)
    FILENAME = sys.argv[1]
   
    imgIn = cv.imread(FILENAME)/255
    if imgIn.size == 0:
        print("failed to load", FILENAME)
        sys.exit(-1)
	
    def update_seuil(x):
        imgOut = imgIn * x / 100
        cv.imshow('kermorvan',imgOut)
		
    # compute output image , canny the image by the trackbar value
    
    cv.namedWindow('kermorvan')
    cv.createTrackbar('mytrackbar','kermorvan',0,100,update_seuil)
    cv.setTrackbarPos('mytrackbar','kermorvan',50)
    


    # outputs
    while True:
        if cv.waitKey() == 27:
            break
        
