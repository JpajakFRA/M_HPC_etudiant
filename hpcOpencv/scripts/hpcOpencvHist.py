#!/usr/bin/env python3


import time
import sys
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot
import cv2 as cv


if __name__ == '__main__':

    
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "<filename>")
        sys.exit(-1)
    FILENAME = sys.argv[1]

    imgIn = cv.imread(FILENAME)
    if imgIn.size == 0:
        print("failed to load", FILENAME)
        sys.exit(-1)

    t0 = time.time()
    color =('b','g','r')
    for i,col in enumerate(color):
        hist = cv.calcHist([imgIn],[i],None,[256],[0,256])
        pyplot.plot(hist,color = col)
        pyplot.xlim([0,256])
        pyplot.yscale('log')
    t1 = time.time()

    pyplot.show()
    print("time:", t1-t0, "s")
   	
